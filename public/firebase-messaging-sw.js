// Scripts for firebase and firebase messaging
// eslint-disable-next-line no-undef
importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js");
// eslint-disable-next-line no-undef
importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js");

// Inicializacion de la aplicación de Firebase en el service worker pasando la configuración generada 
const firebaseConfig = {
  apiKey: "FROM FIREBASE CONSOLE",
  authDomain: "FROM FIREBASE CONSOLE",
  databaseURL: "FROM FIREBASE CONSOLE",
  projectId: "FROM FIREBASE CONSOLE",
  storageBucket: "FROM FIREBASE CONSOLE",
  messagingSenderId: "FROM FIREBASE CONSOLE",
  appId: "FROM FIREBASE CONSOLE",
  measurementId: "FROM FIREBASE CONSOLE",
};

firebase.initializeApp(firebaseConfig);

// Retrieve firebase messaging

// Configuracion para crear notificaciones con campos de título, ícono y cuerpo.

const messaging = firebase.messaging();

messaging.onBackgroundMessage(function (payload) {
  console.log("Received background message ", payload);

  const notificationTitle = payload.notification.title;
  const notificationOptions = {
    body: payload.notification.body,
    icon: "/logo192.png",
  };

  // eslint-disable-next-line no-restricted-globals
  return self.registration.showNotification(
    notificationTitle,
    notificationOptions
  );
});
