import firebase from "firebase/app";
import "firebase/messaging";

// Confirguracion para inicializar el firebase

const firebaseConfig = {
    apiKey: "AIzaSyDtI2Zf_0jAYq9TYrgDdAk3iAjwMNkJS0I",
    authDomain: "push-notification-ee786.firebaseapp.com",
    projectId: "push-notification-ee786",
    storageBucket: "push-notification-ee786.appspot.com",
    messagingSenderId: "258325717630",
    appId: "1:258325717630:web:66687c0d9adf977d0fae2f",
    measurementId: "G-JM5PE4FBD5"
};

firebase.initializeApp(firebaseConfig);

// Configuracion de Cloud messaging para recibir mensajes de notificacion

const messaging = firebase.messaging();

// Configuracion credenciales web - token para mensajes

const { REACT_APP_VAPID_KEY } = process.env;
const publicKey = REACT_APP_VAPID_KEY;

// Configuracion para acceder al token de registro: 
//Devuelve un registro Firebase mensajería en la nube de fichas que se pueden utilizar para enviar mensajes de inserción a la mensajería ejemplo.

export const getToken = async()=>{
  let currentToken = "";

  try {
      currentToken = await messaging.getToken({ vapidKey: publicKey });
      if(currentToken){
          console.log('success token',currentToken)
      } else {
          console.log('failed token',currentToken)
      }
  } catch (error) {
      console.log('Ocurrio un error obteniendo el token', error)
  }
  return currentToken
}


// On Message: recibir los mensajes en primer plano

export const onMessageListener = () =>
  new Promise((resolve) => {
    messaging.onMessage((payload) => {
      resolve(payload);
    });
  });
