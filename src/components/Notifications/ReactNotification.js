import React from "react";
import PropTypes from "prop-types";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const ReactNotificationComponent = ({ title, body }) => {
  let hideNotif = title === "";

  if (!hideNotif) {
    toast(<Display />);
  }

// Se retornan los datos del titulo y cuerpo asignados a la notificacion

  function Display() {
    return (
      <div>
        <h4>{title}</h4>
        <p>{body}</p>
      </div>
    );
  }

// Estilo y tiempo de la notificacion en primer plano

  return (
    <ToastContainer
      autoClose={3000}
      //hideProgressBar
      newestOnTop={false}
      closeOnClick
      rtl={false}
      pauseOnFocusLoss={false}
      draggable
      pauseOnHover={false}
    />
  );
};

// se asigna por defecto el valor del titulo y cuerpo de la notificacion

ReactNotificationComponent.defaultProps = {
  title: "This is title",
  body: "Some body",
};

// Se asigna el tipo de dato al titulo y cuerpo de la notificacion

ReactNotificationComponent.propTypes = {
  title: PropTypes.string,
  body: PropTypes.string,
};

export default ReactNotificationComponent;
